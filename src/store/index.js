import Vue from 'vue'
import Vuex from 'vuex'
import weather from './weather.store'

Vue.use(Vuex)

const store = new Vuex.Store({
  namespaced: true,
  modules: {
    weather
  }
})

export default store
