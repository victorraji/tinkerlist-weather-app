// Import HTTP client
import {
  getWeatherReportService,
  getWeatherInfoService,
  getHistoricalWeatherService
} from '@/services/weatherService'

/** mutation types */
const setCurrentWeatherInfo = 'setCurrentWeatherInfo'
const setWeatherData = 'setWeatherData'
const setWeatherInfo = 'setWeatherInfo'
const setLocationCoords = 'setLocationCoords'
const setNextDaysWeatherInfo = 'setNextDaysWeatherInfo'
const setAPreviousDayWeatherInfo = 'setAPreviousDayWeatherInfo'
const setAllPreviousDaysWeatherInfo = 'setAllPreviousDaysWeatherInfo'
const clearHistoricalData = 'clearHistoricalData'
const setSearchInput = 'setSearchInput'
const setIsGettingAllData = 'setIsGettingAllData'
const setError = 'setError'

/** state */
const state = {
  weatherData: {},
  currentWeatherInfo: {},
  nextDaysWeatherInfo: [],
  locationCoordinates: {},
  weatherInfo: {},
  previousDaysWeatherInfo: [],
  searchInput: '',
  isGettingAllData: true,
  errorPage: null
}

/** getters */
const getters = {
  weatherData (state) {
    return state.weatherData
  },
  weatherInfo (state) {
    return state.weatherInfo
  },
  currentWeatherInfo (state) {
    return state.currentWeatherInfo
  },
  nextDaysWeatherInfo (state) {
    return state.nextDaysWeatherInfo
  },
  previousDaysWeatherInfo (state) {
    return state.previousDaysWeatherInfo
  },
  isGettingAllData (state) {
    return state.isGettingAllData
  },
  searchInput (state) {
    return state.searchInput
  },
  errorPage (state) {
    return state.errorPage
  }
}

/** mutations */
const mutations = {
  [setCurrentWeatherInfo] (state, data) {
    state.currentWeatherInfo = data
  },
  [setWeatherData] (state, data) {
    state.weatherData = data
  },
  [setLocationCoords] (state, data) {
    state.locationCoordinates = data
  },
  [setWeatherInfo] (state, data) {
    state.weatherInfo = data
  },
  [setNextDaysWeatherInfo] (state, data) {
    state.nextDaysWeatherInfo = data
  },
  [setAPreviousDayWeatherInfo] (state, data) {
    state.previousDaysWeatherInfo = [data]
  },
  [setAllPreviousDaysWeatherInfo] (state, data) {
    state.previousDaysWeatherInfo.push(data)
  },
  [clearHistoricalData] (state) {
    state.previousDaysWeatherInfo = []
  },
  [setSearchInput] (state, data) {
    state.searchInput = data
  },
  [setIsGettingAllData] (state, data) {
    state.isGettingAllData = data
  },
  [setError] (state, data) {
    state.errorPage = data
  }
}

const actions = {
  getWeatherReports: ({ commit }, payload) => {
    return getWeatherReportService(payload).then((response) => {
      const reports = {
        name: response.data.name,
        tempMin: response.data.main.temp_min,
        tempMax: response.data.main.temp_max,
        country: response.data.sys.country
      }
      commit('setError', null)
      commit('setWeatherInfo', reports)
      commit('setLocationCoords', response.data.coord)
    })
  },
  getHistoricalWeatherReports: ({ commit }, payload) => {
    return getHistoricalWeatherService(payload.date).then((response) => {
      if (payload.type !== 'all') {
        commit('setAPreviousDayWeatherInfo', response.data.current)
      } else {
        commit('setAllPreviousDaysWeatherInfo', response.data.current)
      }
      commit('setError', null)
    })
  },
  getWeatherInfo: ({ commit }) => {
    return getWeatherInfoService().then((response) => {
      const currentInfo = {
        pressure: response.data.current.pressure,
        feelsLike: response.data.current.feels_like,
        description: response.data.current.weather[0].description,
        icon: response.data.current.weather[0].icon.substring(0, 2),
        main: response.data.current.weather[0].main,
        wind: response.data.current.wind_speed,
        humidity: response.data.current.humidity,
        sunrise: response.data.current.sunrise,
        sunset: response.data.current.sunrise,
        clouds: response.data.current.clouds,
        temp: response.data.current.temp,
        timeZone: response.data.timezone,
        time: response.data.current.dt * 1000
      }
      commit('setError', null)
      commit('setCurrentWeatherInfo', currentInfo)
      commit('setNextDaysWeatherInfo', response.data.daily)
    })
  },
  clearHistoricalDataState: ({ commit }) => {
    commit('clearHistoricalData')
  },
  setCoordinates: ({ commit }, payload) => {
    commit('setLocationCoords', payload)
  },
  setSearch: ({ commit }, payload) => {
    commit('setSearchInput', payload)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
