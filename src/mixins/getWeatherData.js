import { FALLBACK_SEARCH_INPUT } from '@/utils/constants'
import { mapGetters } from 'vuex'
import moment from 'moment'
export default {
  data () {
    return {
      fallBackInput: FALLBACK_SEARCH_INPUT,
      isLoading: false,
      moreWeatherDetails: false,
      selectedDay: 'all',
      daysOptions: [
        {
          label: 'Yesterday',
          value: 1
        },
        {
          label: '2 Days ago',
          value: 2
        },
        {
          label: '3 Days ago',
          value: 3
        },
        {
          label: '4 Days ago',
          value: 4
        },
        {
          label: '5 Days ago',
          value: 5
        },
        {
          label: 'Last Five Days',
          value: 'all'
        }
      ]
    }
  },
  computed: {
    ...mapGetters({
      weatherData: 'weather/weatherReports',
      currentWeatherInfo: 'weather/currentWeatherInfo',
      weatherInfo: 'weather/weatherInfo',
      nextDaysWeatherInfo: 'weather/nextDaysWeatherInfo',
      previousDaysWeatherInfo: 'weather/previousDaysWeatherInfo',
      searchInput: 'weather/searchInput'
    })
  },
  methods: {
    async getPrevWeatherData () {
      const day = this.selectedDay
      if (day === 'all' || day === undefined) {
        this.getAllHistoricalData()
      } else {
        this.historicalWeatherData(day, 'single')
      }
    },
    async getWeatherData () {
      this.isLoading = true
      try {
        await this.$store.dispatch(
          'weather/getWeatherReports',
          this.searchInput
        )
      } catch (err) {
        this.$store.commit('weather/setError', err.response.data.message)
        this.isLoading = false
        return
      }
      await this.getWeatherInfo()
      this.isLoading = false
    },
    async getWeatherInfo () {
      try {
        await this.$store.dispatch('weather/getWeatherInfo')
      } catch (err) {
        this.$store.commit('weather/setError', err.response.data.message)
        return
      }
      this.selectedDay = 'all'
      this.getPrevWeatherData()
      this.$store.commit('weather/setIsGettingAllData', false)
    },
    getCityFromTimeZone (string) {
      const arr = string.split('/')
      return arr[1]
    },
    async getWeatherDataFromTz () {
      await this.getWeatherInfo()
      this.$store.dispatch('weather/setSearch', this.getCityFromTimeZone(this.currentWeatherInfo.timeZone))
      await this.$store.dispatch(
        'weather/getWeatherReports',
        this.getCityFromTimeZone(this.currentWeatherInfo.timeZone)
      )
    },
    async historicalWeatherData (daysAgo, type) {
      const date = moment()
        .add(-1 * daysAgo, 'days')
        .unix()
      const payload = {
        type,
        date
      }
      try {
        await this.$store.dispatch(
          'weather/getHistoricalWeatherReports',
          payload
        )
      } catch (err) {
        this.$store.commit('weather/setError', err.response.data.message)
      }
    },
    async getAllHistoricalData () {
      await this.$store.dispatch('weather/clearHistoricalDataState')
      for (let i = 1; i <= 5; i++) {
        await this.historicalWeatherData(i, 'all')
      }
    },
    async getLocation () {
      const success = (position) => {
        const coords = {
          lon: position.coords.longitude,
          lat: position.coords.latitude
        }
        this.$store.dispatch('weather/setCoordinates', coords)
        this.getWeatherDataFromTz()
      }
      const error = (err) => {
        console.log(err)
        this.$store.dispatch('weather/setSearch', this.fallBackInput)
        this.getWeatherData()
      }
      await navigator.geolocation.getCurrentPosition(success, error)
    }
  }
}
