import axios from 'axios'
import store from '../store'

const baseUrl = 'https://api.openweathermap.org/data/2.5'
const apiKey = 'e2196f855840a6aa017337d3863f17cc'

export async function getWeatherReportService (searchQuery) {
  const response = await axios.get(
    `${baseUrl}/weather?q=${searchQuery}&units=metric&APPID=${apiKey}`
  )
  return response
}
export async function getWeatherInfoService () {
  const { lat, lon } = store.state.weather.locationCoordinates
  const response = await axios.get(
    `${baseUrl}/onecall?lat=${lat}&lon=${lon}&appid=${apiKey}&units=metric&lang=en`
  )
  return response
}
export async function getHistoricalWeatherService (dt) {
  const { lat, lon } = store.state.weather.locationCoordinates
  const response = await axios.get(
    `${baseUrl}/onecall/timemachine?lat=${lat}&lon=${lon}&appid=${apiKey}&dt=${dt}&units=metric&lang=en`
  )
  return response
}
